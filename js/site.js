jQuery(document).ready(function($) {
	$('.btn-video').click(function(event) {
        event.preventDefault();
        var dataVideo = $(this).attr('data-video');
        $('.video-overlay > div > div').append('<iframe width="560" height="315" src="https://www.youtube.com/embed/'+dataVideo+'?autoplay=1" frameborder="0" allowfullscreen></iframe>');
        $('.video-overlay').addClass('is-visible');
     });
    $('.btn-video').trigger('click');
     $('.video-overlay').click(function(ev){
        if( $(ev.target).parents('iframe').length == 0 ) {
            $('.video-overlay').removeClass('is-visible');
            setTimeout(function(){
              $('.video-overlay iframe').remove();
            }, 400);
        }
    });

     $('#btn-topo').click(function() {
     	$('body, html').animate({
     		scrollTop: 0
     	}, 400);
     });


    var vw;
    var vh;
    var timeoutIntro = 300;
    var dimensoes = function () {
        vw = jQuery(window).innerWidth();   
        vh = jQuery(window).innerHeight();
        jQuery('.site-sec').each(function () {
            var distY = jQuery(this).offset().top;
            jQuery(this).attr('data-top', distY);
        });
    }
    dimensoes();


    jQuery(window).resize(function () {
        dimensoes();
    });

    jQuery(window).on('scroll  keyup', function (e) {
        console.log('scroll');
        setTimeout(function(){
            var wST = jQuery(window).scrollTop();
                jQuery('.site-sec').each(function () {
                var that = $(this);
                var distY = that.attr('data-top');
                var maxDist = that.innerHeight() + parseInt(distY);
                
                setTimeout(function(){
                    if(distY <= (wST+(vh/2) + 200) && wST < maxDist ) {
                        that.addClass('is-current');
                    }
                    else {
                        that.removeClass('is-current');
                    }
                }, timeoutIntro);
            });
        }, 200);
        
     });




});